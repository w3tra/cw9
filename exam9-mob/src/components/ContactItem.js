import React from 'react';
import {Image, Text, TouchableNativeFeedback, View, StyleSheet} from "react-native";

const styles = StyleSheet.create({
  contactItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 10,
    marginBottom: 15,
    width: '100%',
    borderWidth: 1,
    borderColor: '#ccc'
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10
  },
  name: {
    fontSize: 15,
    marginRight: 30
  }
});

const ContactItem = (props) => {
  return(
    <TouchableNativeFeedback onPress={props.clicked}>
      <View style={styles.contactItem}>
        <Image resizeMode='contain' source={{uri: props.image}} style={styles.image} />
        <Text style={styles.name}>{props.name}</Text>
      </View>
    </TouchableNativeFeedback>
  );
};

export default ContactItem;
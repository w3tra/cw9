import axios from 'axios';

const url = 'https://blog-187e7.firebaseio.com';

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

export const GET_ID = 'GET_ID';

export const getContacts = () => dispatch => {
  dispatch(fetchPostRequest());
  axios.get(`${url}/contacts.json`)
  .then(response => {
    dispatch(fetchPostSuccess(response.data));
  }, error => {
    dispatch(fetchPostFailure(error));
  });
};

export const getId = (id) => {
  return {type: GET_ID, id};
};

const fetchPostRequest = () => {
  return {type: FETCH_POST_REQUEST};
};

const fetchPostSuccess = (data) => {
  return {type: FETCH_POST_SUCCESS, data};
};

const fetchPostFailure = (error) => {
  return {type: FETCH_POST_FAILURE, error};
};

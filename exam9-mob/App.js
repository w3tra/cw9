import React from 'react';
import PhoneBook from "./src/containers/PhoneBook";
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './src/store/reducer';

const store = createStore(reducer, applyMiddleware(thunk));

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PhoneBook/>
      </Provider>
    );
  }
}

import React, { Component } from 'react';
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import './EditContact.css';
import {Link} from "react-router-dom";
import {editContact} from "../../store/actions/action";

class EditContact extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    photo: ''
  };

  valueChanged = (e) => {
    const name = e.target.name;
    this.setState({[name]: e.target.value});
  };

  editContactHandler = (event) => {
    event.preventDefault();
    const newContact = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      photo: this.state.photo
    };
    this.props.editContact(newContact, this.props.id);
  };

  componentDidMount() {
    this.setState({
      name: this.props.contacts[this.props.id].name,
      email: this.props.contacts[this.props.id].email,
      phone: this.props.contacts[this.props.id].phone,
      photo: this.props.contacts[this.props.id].photo})
  };

  render() {
    return (
      <div className="ContactData">
        <h2>Edit contact form</h2>
        <form>
          <div className="photo-img">
            <span>Photo preview</span>
            <div><img src={this.state.photo ? this.state.photo : null} alt="contact" width="150" height="100"/></div>
          </div>
          <input className="Input" type="text" name="name" placeholder="Your Name"
                 value={this.state.name} onChange={this.valueChanged}/>
          <input className="Input" type="number" name="phone" placeholder="Your Phone"
                 value={this.state.phone} onChange={this.valueChanged}/>
          <input className="Input" type="email" name="email" placeholder="Your Mail"
                 value={this.state.email} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="photo" placeholder="Your Photo"
                 value={this.state.photo} onChange={this.valueChanged}/>

          <Button variant="contained" size="small" className="btn" style={{margin: '5px 10px'}}
                  color="primary" onClick={this.editContactHandler}>Save</Button>
          <Button variant="contained" size="small" style={{margin: '5px 10px'}}
                  component={Link} to="/">Back to contacts</Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    id: state.id
  }
};

const mapDispatchToProps = dispatch => {
  return {
    editContact: (newContact, id) => dispatch(editContact(newContact, id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);
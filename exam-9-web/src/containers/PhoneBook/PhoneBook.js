import React, { Component, Fragment } from 'react';
import {connect} from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import {getContacts, getId} from "../../store/actions/action";
import './PhoneBook.css';
import Modal from "../../components/Modal/Modal";
import ContactView from "../../components/ContactView/ContactView";

class PhoneBook extends Component {

  state = {
    showModal: false
  };

  componentDidMount() {
    this.props.getContacts();
  }

  showModalHandler = (id) => {
    this.setState({showModal: true});
    this.props.getId(id);
  };

  closeModalHandler = () => {
    this.setState({showModal: false});
  };

  render() {
    return(
      <Fragment>
        <div className="PhoneBook-container">
          { !this.props.loading
            ? Object.keys(this.props.contacts).map(itemId => {
              return (
                <div className="contact-item" key={itemId} onClick={() => this.showModalHandler(itemId)}>
                  <img src={this.props.contacts[itemId].photo} alt="contact" width="150" height="100"/>
                  <h1>{this.props.contacts[itemId].name}</h1>
                </div> )
            })
            : <Spinner/> }
        </div>
        <Modal show={this.state.showModal} closed={this.closeModalHandler}>
          <ContactView />
        </Modal>
      </Fragment>

    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    loading: state.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContacts()),
    getId: (id) => dispatch(getId(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneBook);
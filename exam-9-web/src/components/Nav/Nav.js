import React from 'react';
import './Nav.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
  return(
    <div className="Nav">
      <div className="logo">Logo</div>
      <ul>
        <li className="Nav-link"><NavLink to="/">Contacts</NavLink></li>
        <li className="Nav-link"><NavLink to="/contact/add">Add new contact</NavLink></li>
      </ul>
    </div>
  )
};

export default Nav;
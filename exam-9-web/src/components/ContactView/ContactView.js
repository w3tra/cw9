import React, {Component, Fragment} from 'react';
import './ContactView.css';
import {Button} from "@material-ui/core";
import {connect} from "react-redux";
import {removeContact} from "../../store/actions/action";
import {Link} from "react-router-dom";

class ContactView extends Component {

  render() {
    return(
      <div className="ContactView">
        {this.props.id ?
          <Fragment>
            <div className="Contact-card">
              <img src={this.props.contacts[this.props.id].photo} alt="contact" width="150" height="100"/>
              <div>
                <h3>{this.props.contacts[this.props.id].name}</h3>
                <p><i className="material-icons">phone_iphone</i>{this.props.contacts[this.props.id].phone}</p>
                <p><i className="material-icons">email</i>{this.props.contacts[this.props.id].email}</p>
              </div>
            </div>
            <div>
            <Button variant = "contained" style={{margin: '10px'}} component={Link} to="/contact/edit">Edit</Button>
            <Button variant="contained" style={{margin: '10px'}}
                    onClick={() => this.props.removeContact(this.props.id)}>Delete</Button>
            </div>
          </Fragment> : null }
      </div> )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    id: state.id
  }
};

const mapDispatchToProps = dispatch => {
  return {
    removeContact: (id) => dispatch(removeContact(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactView);